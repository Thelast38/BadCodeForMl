# =================================================
#  РЎСЃС‹Р»РєРё/Р·РЅР°С‡РµРЅРёСЏ (РїРѕРґСЂРѕР±РЅРµРµ РѕР± РёР·РјРµРЅСЏРµРјС‹С… С‚РёРїР°С…)
# =================================================

n1 = 2
n2 = n1
n1 = 4
print("n1 = ", n1, "n2 = ", n2)

l1 = [1, 2, 3]
l2 = l1
l2.append(4)
print("l1 = ", l1, "l2 = ", l2)

# РџРµСЂРµРјРµРЅРЅР°СЏ (РІ python) - СЌС‚Рѕ РІСЃРµРіРѕ Р»РёС€СЊ СѓРєР°Р·Р°С‚РµР»СЊ РЅР° РѕР±СЉРµРєС‚ РІ РїР°РјСЏС‚Рё.
# Р•СЃР»Рё РЅРµСЃРєРѕР»СЊРєРѕ РїРµСЂРµРјРµРЅРЅС‹С… СѓРєР°Р·С‹РІР°СЋС‚ РЅР° РѕРґРёРЅ Рё С‚РѕС‚ Р¶Рµ Р�Р—РњР•РќРЇР•РњР«Р™ РѕР±СЉРµРєС‚,
# С‚Рѕ, РёР·РјРµРЅРёРІ РѕР±СЉРµС‚ РїРѕ РѕРґРЅРѕР№ РёР· СЃСЃС‹Р»РѕРє, РјС‹ РјРµРЅСЏРµРј РµРіРѕ РґР»СЏ РІСЃРµС… РѕСЃС‚Р°Р»СЊРЅС‹С….

# Р­С‚Рѕ РѕСЃРѕР±Рѕ РІР°Р¶РЅРѕ РїРѕРЅРёРјР°С‚СЊ РїСЂРё РїРµСЂРµРґР°С‡Рµ РёР·РјРµРЅСЏРµРјС‹С… РѕР±СЉРµРєС‚РѕРІ РІ С„СѓРЅРєС†РёСЋ
# Рё РїСЂРё РёР·РјРµРЅРµРЅРёРё РѕР±СЉРµРєС‚Р° РІ С†РёРєР»Рµ for in (РєРѕС‚РѕСЂС‹Р№ РёС‚РµСЂРёСЂСѓРµС‚ РґР°РЅРЅС‹Р№ РѕР±СЉРµРєС‚)


def modify(lst):
    # Р›СѓС‡С€Рµ РґРµР»Р°С‚СЊ РєРѕРїРёСЋ СЃРїРёСЃРєР° РІРЅСѓС‚СЂРё С„СѓРЅРєС†РёРё
    # lst = lst[:]
    lst.append("new")
    return lst


my_list = [1, 2, 3]
mod_list = modify(my_list)

# Р¤СѓРЅРєС†РёСЏ РІРµСЂРЅСѓР»Р° РёР·РјРµРЅРµРЅРЅС‹Р№ СЃРїРёСЃРѕРє
print('mod_list = ', mod_list)
# РќРѕ РёСЃС…РѕРґРЅС‹Р№ СЃРїРёСЃРѕРє С‚РѕР¶Рµ РёР·РјРµРЅРёР»СЃСЏ, РїРѕРґРѕР±РЅРѕРµ РЅРµСЏРІРЅРѕРµ РїРѕРІРµРґРµРЅРёРµ РЅРµР¶РµР»Р°С‚РµР»СЊРЅРѕ РґР»СЏ С„СѓРЅРєС†РёР№
print('my_list = ', my_list)

# Р‘СѓРґСЊС‚Рµ Р°РєРєСѓСЂР°С‚РЅС‹ РїСЂРё СЂР°Р±РѕС‚Рµ СЃ РёР·РјРµРЅСЏРµРјС‹РјРё РѕР±СЉРµРєС‚Р°РјРё, Р»СѓС‡С€Рµ СЂР°Р±РѕС‚Р°Р№С‚Рµ СЃ РёС… РєРѕРїРёСЏРјРё
mod_list = modify(my_list)

my_list = [1, 2, 3]
# РўРµРїРµСЂСЊ С„СѓРЅРєС†РёСЏ РїРѕР»СѓС‡РёС‚ РєРѕРїРёСЋ СЃРїРёСЃРєР° Рё РЅРµ РёР·РјРµРЅРёС‚ РёСЃС…РѕРґРЅС‹Р№
mod_list = modify(my_list[:])
print('mod_list = ', mod_list)
print('my_list = ', my_list)

my_list = [1, -2, -4, 0, 5, -2]

# РЈРґР°Р»СЏРµРј РІСЃРµ РѕС‚СЂРёС†Р°С‚РµР»СЊРЅС‹Рµ СЌР»РµРјРµРЅС‚С‹
for el in my_list:
    if el < 0:
        my_list.remove(el)

# Р”СѓРјР°СЋ, СЌС‚Рѕ РЅРµ С‚РѕС‚ СЂРµР·СѓР»СЊС‚Р°С‚, РєРѕС‚РѕСЂРѕРіРѕ РІС‹ РѕР¶РёРґР°Р»Рё
print("1)my_list after remove -->", my_list)

my_list = [1, -2, -4, 0, 5, -2]

# Р�С‚РµСЂРёСЂСѓРµРј РїРѕ РєРѕРїРёРё, Р° СѓРґР°Р»СЏРµРј РёР· РѕСЂРёРіРёРЅР°Р»Р°
for el in my_list[:]:
    if el < 0:
        my_list.remove(el)

# Рђ РєР°Рє С…РѕСЂРѕС€Рѕ
print("2)my_list after remove -->", my_list)

# Р•СЃР»Рё РЅСѓР¶РЅРѕ СЃРґРµР»Р°С‚СЊ РїРѕР»РЅСѓСЋ РєРѕРїРёСЋ, СЃРѕ РІСЃРµРјРё РІР»РѕР¶РµРЅРЅС‹РјРё РёР·РјРµРЅСЏРµРјС‹РјРё РѕР±СЉРµРєС‚Р°РјРё, РёСЃРїРѕР»СЊР·СѓРµРј РјРѕРґСѓР»СЊ copy
import copy

l = [[2, 3], [4, 6, [7, 8]]]
l_copy = copy.deepcopy(l)
l[0].append(10)

print('l_copy = ', l_copy)