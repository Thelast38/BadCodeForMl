# =================================
#        РћР±Р»Р°СЃС‚СЊ РІРёРґРёРјРѕСЃС‚Рё
# =================================

# Р�Р·РѕР»РёСЂСѓРµРјС‹Рµ РѕР±Р»Р°СЃС‚Рё РІРёРґРёРјРѕСЃС‚Рё РїРµСЂРµРјРµРЅРЅС‹С… РІ python СЃРѕР·РґР°СЋС‚ С‚РѕР»СЊРєРѕ С„СѓРЅРєС†РёРё
# Р’ python РµСЃС‚СЊ 4 РѕР±Р»Р°СЃС‚Рё РІРёРґРёРјРѕСЃС‚Рё:
# 1/ Р›РѕРєР°Р»СЊРЅР°СЏ
# 2/ РћР±СЉРµРјР»СЋС‰РµР№ С„СѓРЅРєС†РёРё
# 3/ Р“Р»РѕР±Р°Р»СЊРЅР°СЏ (РјРѕРґСѓР»СЏ)
# 4/ Р’СЃС‚СЂРѕРµРЅРЅР°СЏ (builtins)

# РџРѕРёСЃРє РїРµСЂРµРјРµРЅРЅРѕР№ РїСЂРѕРёСЃС…РѕРґРёС‚ РїРѕРѕС‡РµСЂРµРґРЅРѕ СЃ 1 РїРѕ 4 РѕР±Р»Р°СЃС‚СЊ

x = 5  # Р“Р»РѕР±Р°Р»СЊРЅР°СЏ РїРµСЂРµРјРµРЅРЅР°СЏ - РґРѕСЃС‚СѓРїРЅР° РІ Р»СЋР±РѕРј РјРµСЃС‚Рµ РґР°РЅРЅРѕРіРѕ РјРѕРґСѓР»СЏ (С„Р°Р№Р»Р°)


def outside():
    y = 10  # РґРѕСЃС‚СѓРїРЅР° РІ С‚РµР»Рµ РґР°РЅРЅРѕР№ С„СѓРЅРєС†РёРё + РІРѕ РІСЃРµС… РІР»РѕР¶РµРЅРЅС‹С…

    def inside():
        z = 15  # РґРѕСЃС‚СѓРїРЅР° С‚РѕР»СЊРєРѕ РІ С‚РµР»Рµ РґР°РЅРЅРѕР№ С„СѓРЅРєС†РёРё
        print('inside x: {}, y: {}, z: {}'.format(x, y, z))

    inside()
    print('outside x: {}, y: {}, z: {}'.format(x, y, 'z РЅРµРґРѕСЃС‚СѓРїРЅР°'))


outside()
print('inside x: {}, y: {}, z: {}'.format(x, 'y РЅРµРґРѕСЃС‚СѓРїРЅР°', 'z РЅРµРґРѕСЃС‚СѓРїРЅР°'))

x = 5


def wrapper():
    def test1():
        x = 10  # Р»РѕРєР°Р»СЊРЅР°СЏ РїРµСЂРµРјРµРЅРЅР°СЏ x РїРµСЂРµРєСЂС‹РІР°РµС‚ РІРёРґРёРјРѕСЃС‚СЊ РіР»РѕР±Р°Р»СЊРЅРѕР№ x
        print('test1 x = ', x)

    def test2():
        print('test2 x = ', x)
        # x = 22  #         ^-- РѕС€РёР±РєР°, РІС‹С€Рµ РёСЃРїРѕР»СЊР·СѓРµРј РїРµСЂРµРјРµРЅРЅСѓСЋ РѕР±СЉСЏРІР»РµРЅРЅСѓСЋ РїРѕР·РґРЅРµРµ

    def test3():
        global x  # РёРЅСЃС‚СЂСѓРєС†РёСЏ global - РїРѕРёСЃРє РїРµСЂРµРјРµРЅРЅРѕР№ РІ РіР»РѕР±Р°Р»СЊРЅРѕР№ РѕР±Р»Р°СЃС‚Рё
        # РўР°Рє Р¶Рµ РµСЃС‚СЊ РёРЅСЃС‚СЂСѓРєС†РёСЏ nonlocal -  РїРѕРёСЃРє РїРµСЂРµРјРµРЅРЅРѕР№ РІ РѕР±СЉРµРјР»СЋС‰РµР№ С„СѓРЅРєС†РёРё
        print('test3 x = ', x)
        x = 25

    test1()
    test2()
    test3()

wrapper()
print('after wrapper x = ', x)